from flask import Flask, render_template
from flask_cors import CORS
app = Flask(__name__, template_folder='www', static_url_path='/static')
CORS(app)
# Ở đây dùng để import các file vào mà sử dụng
from mainapp.api import all_blueprints
for bp in all_blueprints:
    app.register_blueprint(bp)

from mainapp.system import sys_blueprints
for bp in sys_blueprints:
    app.register_blueprint(bp)

from mainapp.sample_code import sample
app.register_blueprint(sample)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def home(path):
    return render_template("index.html")


if __name__ == "__main__":
    app.run(port=5799, debug=True)
