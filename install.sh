#!/bin/bash
################################################################################
# Script for installing system
# Author: UNIC JSC
#-------------------------------------------------------------------------------
# This script will install the backend software on the server
#-------------------------------------------------------------------------------
# Execute the script to install:
# sudo chmod +x install.sh
# ./install
################################################################################

OE_USER="lenk"
OE_DBNAME="mhm"
OE_DBPASS="123123"

export LANGUAGE="en_US.UTF-8"
sudo sh -c "echo 'LANGUAGE="en_US.UTF-8"' >> /etc/default/locale"
sudo sh -c "echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale"

export LC_ALL=C
source ~/.bashrc

#--------------------------------------------------
# Update Server
#--------------------------------------------------
echo -e "\n---- Update Server ----"

sudo apt-get update
sudo apt-get upgrade -y

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------
echo -e "\n---- Install PostgreSQL Server ----"
# sudo apt-get install postgresql -y
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

RELEASE=$(lsb_release -cs)
# echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

sudo apt update
sudo apt -y install postgresql-11

echo -e "\n---- Creating the User  ----"
sudo su - postgres -c "createuser -s $OE_USER" 2> /dev/null || true
sudo -u postgres psql -c "ALTER USER $OE_USER PASSWORD '$OE_DBPASS'" 2> /dev/null || true
sudo -u postgres psql -c "create database $OE_DBNAME" 2> /dev/null || true
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE '$OE_DBNAME' to $OE_USER" 2> /dev/null || true

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
echo -e "\n--- Installing Python 3 + pip3 --"
sudo apt-get install python3 python3-pip -y
sudo apt-get install supervisor -y
apt-get install lsof -y
sudo pip3 install flask flask-cors pandas psycopg2 gunicorn
sudo pip3 install python-crontab
sudo pip3 install greenlet
sudo pip3 install eventlet
sudo pip3 install gunicorn[eventlet]
sudo pip3 install gevent
sudo pip3 install gunicorn[gevent]

echo -e "\n---- Create directory ----"
sudo mkdir /backend
sudo chmod -R 777 /backend

git clone https://lengockhoa@bitbucket.org/lengockhoa/unicpy.git
sudo cp -R unicpy/* /backend/
sudo rm -rf unicpy
sudo chmod +x /backend/run.sh

sudo sh -c "echo '@reboot root /backend/run.sh' >> /etc/crontab"
sudo service cron restart

sudo sh -c "echo 'host  all     all     all     md5' >> /etc/postgresql/11/main/pg_hba.conf"
sudo sh -c "echo listen_addresses = \'*\' >> /etc/postgresql/11/main/postgresql.conf"
sudo /etc/init.d/postgresql restart
sudo /backend/run.sh

echo -e "\n--- Chu y: Cai dat DB sau khi chay script nhe --"
echo -e "\n--- User: $OE_USER, Pass: $OE_DBPASS, db_name: $OE_DBNAME --"
