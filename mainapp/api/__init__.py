from mainapp.api.users import users
from mainapp.api.machine import machines
from mainapp.api.schedule import schedules
from mainapp.api.task import tasks
from mainapp.api.temperature import temperatures
from mainapp.api.vibration import vibrations
from mainapp.api.visual import visuals
from mainapp.api.category import categorys
from mainapp.api.masterdata import masterdatas
from mainapp.api.md5string import md5strings
from mainapp.api.inputdata import inputs
from mainapp.api.inspectgroup import inspectgroups
from mainapp.api.plant import plants
from mainapp.api.outputdata import outputs
from mainapp.api.categorygroup import categorygroups

all_blueprints = [users, machines, schedules, tasks,
                  temperatures, vibrations, visuals, categorys,
                  masterdatas, md5strings, inputs, inspectgroups,
                  plants, outputs, categorygroups]
