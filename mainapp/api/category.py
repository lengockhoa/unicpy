from mainapp.models.m_category import Category
from flask import Blueprint, jsonify, request, abort
categorys = Blueprint('categorys', __name__)
import json


@categorys.route('/category/add', methods=['POST'])
def category_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        category = Category()
        category.code = val['code']
        category.name = val['name']
        category.name_vn = val['name_vn']
        category.machine_code = val['machine_code']
        category.type = val['type']
        category.categorygroup_code = val['categorygroup_code']
        retval = category.select()
        categoryselect = json.loads(retval)
        if len(categoryselect) == 1:
            result = category.update()
        else:
            result = category.insert()
        return jsonify({'successful': result}), 201


@categorys.route('/category/remove', methods=['POST'])
def category_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        category = Category()
        category.code = val['code']
        result = category.remove()
        return jsonify({'successful': result}), 201


@categorys.route('/category/select', methods=['POST'])
def category_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        category = Category()
        category.code = val['code']
        result = category.select()
        return result, 201


@categorys.route('/category/selectall', methods=['POST'])
def category_selectall():
    category = Category()
    result = category.selectall()
    return result, 201


