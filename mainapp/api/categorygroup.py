from mainapp.models.m_categorygroup import CategoryGroup
from flask import Blueprint, jsonify, request, abort
categorygroups = Blueprint('categorygroups', __name__)
import json


@categorygroups.route('/categorygroup/add', methods=['POST'])
def categorygroup_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        categorygroup = CategoryGroup()
        categorygroup.code = val['code']
        categorygroup.name = val['name']
        retval = categorygroup.select()
        categorygroupeselect = json.loads(retval)
        if len(categorygroupeselect) == 1:
            result = categorygroup.update()
        else:
            result = categorygroup.insert()
        return jsonify({'successful': result}), 201


@categorygroups.route('/categorygroup/remove', methods=['POST'])
def categorygroup_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        categorygroup = CategoryGroup()
        categorygroup.code = val['code']
        result = categorygroup.remove()
        return jsonify({'successful': result}), 201


@categorygroups.route('/categorygroup/select', methods=['POST'])
def categorygroup_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        categorygroup = CategoryGroup()
        categorygroup.code = val['code']
        result = categorygroup.select()
        return result, 201


@categorygroups.route('/categorygroup/selectfrommachinecode', methods=['POST'])
def categorygroup_selectfrommachinecode():
    if not request.json:
        abort(400)
    else:
        val = request.json
        categorygroup = CategoryGroup()
        result = categorygroup.selectfrommachinecode(val['machine_code'])
        return result, 201


@categorygroups.route('/categorygroup/selectall', methods=['POST'])
def categorygroup_selectall():
    categorygroup = CategoryGroup()
    categorygroupeall = categorygroup.selectall()
    return categorygroupeall, 201
