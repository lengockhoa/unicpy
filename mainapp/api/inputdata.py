from mainapp.models.m_inputdata import DTask, DVisual, DVibration, DTemperature, DPicture
from mainapp.models.m_task import Task
from mainapp.models.m_users import Users
from flask import Blueprint, jsonify, request
from werkzeug.utils import secure_filename
inputs = Blueprint('inputs', __name__)
import json
import os
from mainapp.config import IMAGE_DIR_FULL
import datetime
from datetime import datetime


@inputs.route('/input/addvisual', methods=['POST'])
def input_addvisual():
    valjson = json.loads(request.form['visual'])
    inspector = Users()
    inspector.username = valjson['username']
    user = inspector.selectbyusername()
    userobj = json.loads(user)
    user_code = userobj[0]['code']
    visual = DVisual()
    visual.q_code = valjson['q_code']
    visual.machine_code = valjson['machine_code']
    visual.rs_answer = valjson['rs_answer']
    visual.rs_comment = valjson['rs_comment']
    visual.date_inspection = valjson['date_inspection']
    visual.category_code = valjson['q_category']
    visual.date_answer = valjson['date_answer']
    visual.inspect_type = valjson['inspect_type']
    visual.user_code = user_code
    task = DTask()
    task.machine_code = valjson['machine_code']
    task.date_inspection = valjson['date_inspection']
    task.username = valjson['username']
    taskselect = task.select()
    taskselect = json.loads(taskselect)
    if visual.inspect_type == 'schedule':
        if len(taskselect) == 1:
            visual.insert()
    else:
        visual.insert()
    return jsonify({'successful': True}), 201


@inputs.route('/input/addvibration', methods=['POST'])
def input_addvibration():
    vibrationdata = json.loads(request.form['vibration'])
    inspector = Users()
    inspector.username = vibrationdata['username']
    user = inspector.selectbyusername()
    userobj = json.loads(user)
    user_code = userobj[0]['code']
    vibration = DVibration()
    vibration.q_code = vibrationdata['q_code']
    vibration.machine_code = vibrationdata['machine_code']
    vibration.rs_h = vibrationdata['rs_h']
    vibration.rs_v = vibrationdata['rs_v']
    vibration.rs_a = vibrationdata['rs_a']
    vibration.date_inspection = vibrationdata['date_inspection']
    vibration.date_answer = vibrationdata['date_answer']
    vibration.inspect_type = vibrationdata['inspect_type']
    vibration.user_code = user_code
    task = DTask()
    task.machine_code = vibrationdata['machine_code']
    task.date_inspection = vibrationdata['date_inspection']
    task.username = vibrationdata['username']
    taskselect = task.select()
    taskselect = json.loads(taskselect)
    if vibration.inspect_type == 'schedule':
        if len(taskselect) == 1:
            vibration.insert()
    else:
        vibration.insert()
    return jsonify({'successful': True}), 201


@inputs.route('/input/addtemperature', methods=['POST'])
def input_addtemperature():
    temperaturedata = json.loads(request.form['temperature'])
    temperature = DTemperature()
    inspector = Users()
    inspector.username = temperaturedata['username']
    user = inspector.selectbyusername()
    userobj = json.loads(user)
    user_code = userobj[0]['code']
    temperature.q_code = temperaturedata['q_code']
    temperature.machine_code = temperaturedata['machine_code']
    temperature.t_value = temperaturedata['t_value']
    temperature.date_inspection = temperaturedata['date_inspection']
    temperature.date_answer = temperaturedata['date_answer']
    temperature.inspect_type = temperaturedata['inspect_type']
    temperature.user_code = user_code
    task = DTask()
    task.machine_code = temperaturedata['machine_code']
    task.date_inspection = temperaturedata['date_inspection']
    task.username = temperaturedata['username']
    taskselect = task.select()
    taskselect = json.loads(taskselect)
    if temperature.inspect_type == 'schedule':
        if len(taskselect) == 1:
            temperature.insert()
    else:
        temperature.insert()
    return jsonify({'successful': True}), 201


@inputs.route('/input/addpicture', methods=['POST'])
def input_addpicture():
    info = json.loads(request.form['info'])
    file = request.files['picture']
    filename = secure_filename(info['file_name'])
    inspector = Users()
    inspector.username = info['username']
    user = inspector.selectbyusername()
    userobj = json.loads(user)
    user_code = userobj[0]['code']
    picture = DPicture()
    picture.q_code = info['q_code']
    picture.category_code = info['q_category']
    picture.machine_code = info['machine_code']
    picture.rs_picture = filename
    picture.date_inspection = info['date_inspection']
    picture.date_answer = info['date_answer']
    picture.inspect_type = info['inspect_type']
    picture.user_code = user_code
    pictureselected = picture.select()
    pictureselected = json.loads(pictureselected)
    if picture.inspect_type == 'schedule':
        if len(pictureselected) == 0:
            picture.insert()
    else:
        picture.insert()
    picture.user_code = user_code
    file.save(os.path.join(IMAGE_DIR_FULL, filename))
    return jsonify({'successful': True}), 201


@inputs.route('/input/updatetask', methods=['POST'])
def input_updatetask():
    info = json.loads(request.form['task'])
    task = Task()
    task.machine_code = info['machine_code']
    task.date_inspection = info['date_inspection']
    task.date_answer = info['date_answer'][:10]
    task.date_inspection = info['date_inspection']
    task.is_checked_temperature = info['inspected_temperature']
    task.is_checked_vibration = info['inspected_vibration']
    task.is_checked_visual = info['inspected_visual']
    task.machine_code = info['machine_code']
    task.not_check = info['not_check']
    task.on_demand = info['on_demand']
    task.reason = info['reason']
    task.username = info['username']
    task.sync_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    taskselected = task.selectbymachineandinspectiondate()
    taskselected = json.loads(taskselected)
    # Tìm username để gán vào nhóm
    inspector = Users()
    inspector.username = info['username']
    user = inspector.selectbyusername()
    userobj = json.loads(user)
    if len(taskselected) == 1:
        if task.on_demand == 'Y':
            # Nếu ondemand mà đã có dữ liệu thì xóa cái cũ và nạp lại cái mới
            visual = DVisual()
            visual.machine_code = info['machine_code']
            visual.date_inspection = info['date_inspection']
            visual.username = info['username']
            visual.removeondemand()
            vibration = DVibration()
            vibration.machine_code = info['machine_code']
            vibration.date_inspection = info['date_inspection']
            vibration.username = info['username']
            vibration.removeondemand()
            temperature = DTemperature()
            temperature.machine_code = info['machine_code']
            temperature.date_inspection = info['date_inspection']
            temperature.username = info['username']
            temperature.removeondemand()
            visualimage = DPicture()
            visualimage.machine_code = info['machine_code']
            visualimage.date_inspection = info['date_inspection']
            visualimage.username = info['username']
            visualimage.removeondemand()
            task.inspectgroup_code = userobj[0]['inspectgroup_code']
            task.task_type = taskselected[0]['task_type']
            task.updatebyuser()
            return jsonify({'successful': True}), 201
        else:
            # Nếu là schedule thì tìm xem có phải NNN để update lại thành YYY
            if taskselected[0]['is_checked_visual'] == 'N' and taskselected[0]['is_checked_vibration'] == 'N' and taskselected[0]['is_checked_temperature'] == 'N':
                task.inspectgroup_code = taskselected[0]['inspectgroup_code']
                task.task_type = taskselected[0]['task_type']
                if task.not_check == 'Y':
                    task.is_checked_temperature = 'C'
                    task.is_checked_vibration = 'C'
                    task.is_checked_visual = 'C'
                task.updatebyuser()
                return jsonify({'successful': True}), 201
            else:
                return jsonify(
                    {'successful': False,
                     'mesage': 'Machine with code ' + taskselected[0]['machine_code'] + ' is synced by ' + taskselected[0]['username']}), 201
    else:
        if task.on_demand == 'Y':
            inspectgroup_code = userobj[0]['inspectgroup_code']
            task.inspectgroup_code = inspectgroup_code
            task.task_type = 'on demand'
            task.insert()
            return jsonify({'successful': True}), 201
        # không có chuyện ko có task mà đi update



