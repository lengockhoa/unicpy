from mainapp.models.m_inspectgroup import InspectGroup
from flask import Blueprint, jsonify, request, abort
inspectgroups = Blueprint('inspectgroups', __name__)
import json


@inspectgroups.route('/inspectgroup/add', methods=['POST'])
def inspectgroup_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        inspectgroup = InspectGroup()
        inspectgroup.group_code = val['group_code']
        inspectgroup.group_name = val['group_name']
        retval = inspectgroup.select()
        inspectgroupeselect = json.loads(retval)
        if len(inspectgroupeselect) == 1:
            result = inspectgroup.update()
        else:
            result = inspectgroup.insert()
        return jsonify({'successful': result}), 201


@inspectgroups.route('/inspectgroup/remove', methods=['POST'])
def inspectgroup_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        inspectgroup = InspectGroup()
        inspectgroup.group_code = val['group_code']
        result = inspectgroup.remove()
        return jsonify({'successful': result}), 201


@inspectgroups.route('/inspectgroup/select', methods=['POST'])
def inspectgroup_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        inspectgroup = InspectGroup()
        inspectgroup.group_code = val['group_code']
        result = inspectgroup.select()
        return result, 201


@inspectgroups.route('/inspectgroup/selectall', methods=['POST'])
def inspectgroup_selectall():
    inspectgroup = InspectGroup()
    inspectgroupeall = inspectgroup.selectall()
    return inspectgroupeall, 201
