from mainapp.models.m_machine import Machine
from werkzeug.utils import secure_filename
from flask import Blueprint, jsonify, request, abort
machines = Blueprint('machines', __name__)
import json
import os
from mainapp.config import IMAGE_MACHINE_FULL


@machines.route('/machine/add', methods=['POST'])
def machine_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        machine = Machine()
        machine.code = val['code']
        machine.name = val['name']
        machine.name_vn = val['name_vn']
        machine.plant_code = val['plant_code']
        machine.machine_class = val['machine_class']
        retval = machine.select()
        machineselect = json.loads(retval)
        if len(machineselect) == 1:
            result = machine.update()
        else:
            result = machine.insert()
        return jsonify({'successful': result}), 201


@machines.route('/machine/updatepicture', methods=['POST'])
def machine_updatepicture():
    code = request.form['code']
    file = request.files['picture']
    filename = secure_filename(file.filename)
    extension = filename.rsplit('.', 1)[1]
    newfilename = code + '.' + extension
    file.save(os.path.join(IMAGE_MACHINE_FULL, newfilename))
    machine = Machine()
    machine.code = code
    machine.picture = newfilename
    machine.updatepicture()
    return jsonify({'successful': True}), 201


@machines.route('/machine/updatepospicture', methods=['POST'])
def machine_updatepospicture():
    code = request.form['code']
    file = request.files['pos_picture']
    filename = secure_filename(file.filename)
    extension = filename.rsplit('.', 1)[1]
    newfilename = 'pos' + code + '.' + extension
    file.save(os.path.join(IMAGE_MACHINE_FULL, newfilename))
    machine = Machine()
    machine.code = code
    machine.pos_picture = newfilename
    machine.updatposepicture()
    return jsonify({'successful': True}), 201


@machines.route('/machine/select', methods=['POST'])
def machine_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        machine = Machine()
        machine.code = val['code']
        result = machine.select()
        return result, 201


@machines.route('/machine/selectall', methods=['POST'])
def machine_selectall():
    machine = Machine()
    result = machine.selectall()
    return result, 201
