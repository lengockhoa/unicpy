from flask import Blueprint, jsonify

masterdatas = Blueprint('masterdatas', __name__)
import os.path
import json
import pandas as pd
from mainapp.config import IMAGE_MACHINE, BASE_URL, ROOT_DIR
from mainapp.models.m_machine import Machine
from mainapp.models.m_temperature import Temperature
from mainapp.models.m_vibration import Vibration
from mainapp.models.m_visual import Visual
from mainapp.models.m_category import Category


@masterdatas.route('/masterdata/getall', methods=['POST'])
def masterdata_getall():
    machine = Machine()
    machineall = machine.selectall()
    dfmachine = pd.read_json(machineall, orient='records')

    dfmachine['picture'] = dfmachine['picture'].apply(lambda x: BASE_URL + IMAGE_MACHINE + '/' + x
    if os.path.isfile(str(ROOT_DIR + '/..' + IMAGE_MACHINE + '/' + x)) == True else '')
    dfmachine['pos_picture'] = dfmachine['pos_picture'].apply(lambda x: BASE_URL + IMAGE_MACHINE + '/' + x
    if os.path.isfile(str(ROOT_DIR + '/..' + IMAGE_MACHINE + '/' + x)) == True else '')

    machineall = dfmachine.to_json(orient='records')

    category = Category()
    category.type = 'VISUAL'
    visualcategory = category.selectbytype()
    categoryall = category.selectall()
    dfcategory = pd.read_json(categoryall, orient='records')

    temperature = Temperature()
    temperatureall = temperature.selectall()
    dftemperature = pd.read_json(temperatureall, orient='records')
    dftemperature = dftemperature.merge(dfcategory, left_on='category_code', right_on='code')
    dftemperature = dftemperature.merge(dfmachine, left_on='machine_code', right_on='code')
    dftemperature = dftemperature[['code_x', 'machine_code', 'name_x', 'name_vn_x', 'standard_alarm_1', 'standard_alarm_2']]
    dftemperature.rename(columns={'code_x': 'code', 'name_x': 'name', 'name_vn_x': 'name_vn'}, inplace=True)
    temperatureall = dftemperature.to_json(orient='records')

    vibration = Vibration()
    vibrationall = vibration.selectall()
    dfvibration = pd.read_json(vibrationall, orient='records')
    dfvibration = dfvibration.merge(dfcategory, left_on='category_code', right_on='code')
    dfvibration = dfvibration.merge(dfmachine, left_on='machine_code', right_on='code')
    dfvibration = dfvibration[['code_x', 'machine_code', 'name_x', 'name_vn_x', 'standard_alarm_1', 'standard_alarm_2']]
    dfvibration.rename(columns={'code_x': 'code', 'name_x': 'name', 'name_vn_x': 'name_vn'}, inplace=True)
    vibrationall = dfvibration.to_json(orient='records')

    visual = Visual()
    visualall = visual.selectall()
    result = {'machine': json.loads(machineall),
              'q_temperature': json.loads(temperatureall),
              'q_vibration': json.loads(vibrationall),
              'q_visual': json.loads(visualall),
              'q_visual_category': json.loads(visualcategory)
              }
    return jsonify(result), 201
