from mainapp.models.m_md5string import Md5string
from flask import Blueprint, jsonify, request, abort
md5strings = Blueprint('md5strings', __name__)
import json


@md5strings.route('/md5string/check', methods=['POST'])
def md5string_add():
    md5string = Md5string()
    md5string.changemanage()
    result = md5string.selectmaster()
    result = json.loads(result)
    return jsonify(result[0]), 201


