from mainapp.models.m_users import Users
from mainapp.models.m_schedule import Schedule
from mainapp.models.m_machine import Machine
from mainapp.models.m_category import Category
from mainapp.models.m_outputdata import DMachineInspect, RawData
from flask import Blueprint, jsonify, request
outputs = Blueprint('outputs', __name__)
import pandas as pd
import numpy as np
import datetime
from datetime import datetime
import json


@outputs.route('/output/schedulelist', methods=['POST'])
def output_schedulelist():
    val = request.json
    from_date = val['from_date']
    to_date = val['to_date']
    user = Users()
    user.username = val['username']
    userselect = user.selectbyusername()
    userselect = json.loads(userselect)
    retval = []
    if len(userselect) == 1:
        schedule = Schedule()
        result = schedule.selectbyusercode(userselect[0]['code'])
        schedulelist = json.loads(result)
        for sche in schedulelist:
            daterange = pd.date_range(from_date, to_date)
            for date in daterange:
                datediff = abs((date - datetime.strptime(sche['begininspect'], "%Y-%m-%d")).days)
                is_today = datediff % sche['numberofdate']
                if is_today == 0:
                    machine_name = ''
                    machine = Machine()
                    machine.code = sche['machine_code']
                    machine_obj = machine.select()
                    machine_obj = json.loads(machine_obj)
                    if len(machine_obj) == 1:
                        machine_name = machine_obj[0]['name']
                    retval.append({'code': sche['machine_code'],
                                   'name': machine_name,
                                   'date_inspection': date.strftime('%Y-%m-%d')
                                   })
    retval.sort(key=lambda c: c['date_inspection'])
    return jsonify(retval), 201


@outputs.route('/output/machinedata', methods=['POST'])
def output_machinedata():
    val = request.json
    user = Users()
    user.username = val['username']
    userselect = user.selectbyusername()
    userselect = json.loads(userselect)
    if len(userselect) == 1:
        machineinspect = DMachineInspect()
        machineinspect.date_inspection = val['date_inspection']
        machineinspect.machine_code = val['machine_code']
        retval = []
        if val['inspect_type'] == 'visual':
            retval = machineinspect.selectvisualbydate()
        if val['inspect_type'] == 'vibration':
            retval = machineinspect.selectvibrationbydate()
        if val['inspect_type'] == 'temperature':
            retval = machineinspect.selecttemperaturebydate()
        return retval, 201


@outputs.route('/output/machinedatadanger', methods=['POST'])
def output_machinedatadanger():
    val = request.json
    user = Users()
    user.username = val['username']
    userselect = user.selectbyusername()
    userselect = json.loads(userselect)
    if len(userselect) == 1:
        machineinspect = DMachineInspect()
        machineinspect.date_inspection = val['date_inspection']
        machineinspect.machine_code = val['machine_code']
        retval = []
        if val['inspect_type'] == 'visual':
            retval = machineinspect.selectvisualbydatedanger()
        if val['inspect_type'] == 'vibration':
            retval = machineinspect.selectvibrationbydatedanger()
        if val['inspect_type'] == 'temperature':
            retval = machineinspect.selecttemperaturebydatedanger()
        return retval, 201


@outputs.route('/output/machinereport', methods=['POST'])
def output_machinereport():
    val = request.json
    user = Users()
    user.username = val['username']
    userselect = user.selectbyusername()
    userselect = json.loads(userselect)
    if len(userselect) == 1:
        inspect = DMachineInspect()
        visual = inspect.calculatevisualtask(val['date_inspection'])
        vibration = inspect.calculatevibrationtask(val['date_inspection'])
        temperature = inspect.calculatetemperaturetask(val['date_inspection'])
        dfvisual = pd.DataFrame(visual)
        dfvibration = pd.DataFrame(vibration)
        dftemperature = pd.DataFrame(temperature)
        if len(dfvibration) == 0 and len(dfvisual) == 0 and len(dftemperature) == 0:
            return jsonify([]), 201
        dfmerge = dfvisual.merge(dfvibration, how="left", left_on='machine_code', right_on='machine_code')
        dfmerge = dfmerge.merge(dftemperature, how="left", left_on='machine_code', right_on='machine_code')
        dfmerge['grade_visual'] = dfmerge['grade_visual'].fillna('C')
        dfmerge['grade_vibration'] = dfmerge['grade_vibration'].fillna('C')
        dfmerge['grade_temperature'] = dfmerge['grade_temperature'].fillna('C')
        dfmerge.rename(columns={'grade_visual': 'visual', 'grade_vibration': 'vibration', 'grade_temperature': 'temperature'}, inplace=True)
        dfmerge['date_inspection'] = val['date_inspection']
        if val.get('machine_code', '') != '':
            dfmerge = dfmerge[dfmerge['machine_code'] == val.get('machine_code', '')]
        conditions = [
            (dfmerge['visual'] == 'A') & (dfmerge['vibration'] == 'A') & (dfmerge['temperature'] == 'A'),
            (dfmerge['visual'] == 'C') | (dfmerge['vibration'] == 'C') | (dfmerge['temperature'] == 'C'),
            (dfmerge['visual'] == 'B') | (dfmerge['vibration'] == 'B') | (dfmerge['temperature'] == 'B'),
            ]
        choices = ['A', 'C', 'B']
        machine = Machine()
        machinelist = machine.selectall()
        dfmachine = pd.read_json(machinelist)
        dfmerge['summary'] = np.select(conditions, choices, default='A')
        dfmerge = dfmerge.merge(dfmachine, how="left", left_on='machine_code', right_on='code')
        dfmerge = dfmerge.to_json(orient='records')
        return dfmerge, 201


@outputs.route('/output/temperaturedata', methods=['POST'])
def output_temperaturedata():
    rawdata = RawData()
    from_date = datetime.now().strftime("%Y-%m-%d")
    to_date = datetime.now().strftime("%Y-%m-%d")
    val = request.json
    from_date = val.get('from_date', from_date)
    to_date = val.get('to_date', to_date)
    temperaturedata = rawdata.gettemperaturedata(from_date, to_date)
    return temperaturedata, 201


@outputs.route('/output/vibrationdata', methods=['POST'])
def output_vibrationdata():
    rawdata = RawData()
    from_date = datetime.now().strftime("%Y-%m-%d")
    to_date = datetime.now().strftime("%Y-%m-%d")
    val = request.json
    from_date = val.get('from_date', from_date)
    to_date = val.get('to_date', to_date)
    vibrationdata = rawdata.getvibrationdata(from_date, to_date)
    return vibrationdata, 201


@outputs.route('/output/visualdata', methods=['POST'])
def output_visualdata():
    rawdata = RawData()
    from_date = datetime.now().strftime("%Y-%m-%d")
    to_date = datetime.now().strftime("%Y-%m-%d")
    val = request.json
    from_date = val.get('from_date', from_date)
    to_date = val.get('to_date', to_date)
    visualdata = rawdata.getvisualdata(from_date, to_date)
    return visualdata, 201


@outputs.route('/output/machinechartdata', methods=['POST'])
def output_machinechartdata():
    rawdata = RawData()
    val = request.json
    date_selected = val['date_selected']
    machine_code = val['machine_code']
    inspect_type = val['inspect_type']
    category_group = val['category_group']
    result = {}
    category = Category()
    categorylist = category.selectbycategorygroup(category_group)
    categorylist = json.loads(categorylist)
    if inspect_type.lower() == 'vibration':
        vibration_data = []
        dfvibration = []
        for cat in categorylist:
            valdata = rawdata.getdataforchartvibration(date_selected, machine_code, cat['code'])
            vibration_data.append(valdata)
        for data in vibration_data:
            dataarr = json.loads(data)
            if len(dataarr) > 0:
                positionname = dataarr[0]['position'].replace(" ", "")
                dftemp = pd.read_json(data)

                dftemp.rename(columns={'rs_h': positionname+'_h',
                                       'rs_v': positionname+'_v',
                                       'rs_a': positionname + '_a'
                                       }, inplace=True)
                dftemp = dftemp.drop(columns=['inspect_type', 'position', 'machine_code'])
                dfvibration.append(dftemp)
        if len(dfvibration) > 1:
            print(dfvibration)
            pdres = dfvibration[0]
            for x in range(1, len(dfvibration)):
                dfvibration[x] = dfvibration[x].drop(columns=['standard_alarm_1', 'standard_alarm_2'])
                pdres = pdres.merge(dfvibration[x], how='left', left_on='date_inspection', right_on='date_inspection')
            result = {'content': json.loads(pdres.to_json(orient='records')),
                      'field': list(pdres)
                      }
        else:

            pdres = dfvibration[0]
            result = {'content': json.loads(pdres.to_json(orient='records')),
                      'field': list(pdres)
                      }
    if inspect_type.lower() == 'temperature':
        temperature_data = []
        dftemperature = []
        for cat in categorylist:
            valdata = rawdata.getdataforcharttemperature(date_selected, machine_code, cat['code'])
            temperature_data.append(valdata)
        for data in temperature_data:
            dataarr = json.loads(data)
            if len(dataarr) > 0:
                positionname = dataarr[0]['position'].replace(" ", "")
                dftemp = pd.read_json(data)
                dftemp.rename(columns={'t_value': positionname+'_value',
                                       }, inplace=True)
                dftemp = dftemp.drop(columns=['code', 'inspect_type', 'position', 'machine_code'])
                dftemperature.append(dftemp)
        if len(dftemperature) > 1:
            pdres = dftemperature[0]
            for x in range(1, len(dftemperature)):
                dftemperature[x] = dftemperature[x].drop(columns=['standard_alarm_1', 'standard_alarm_2'])
                pdres = pdres.merge(dftemperature[x], how='left', left_on='date_inspection', right_on='date_inspection')
            result = {'content': json.loads(pdres.to_json(orient='records')),
                      'field': list(pdres)
                      }
        if len(dftemperature) == 1:
            pdres = dftemperature[0]
            result = {'content': json.loads(pdres.to_json(orient='records')),
                      'field': list(pdres)
                      }
        if len(dftemperature) == 0:
            result = {'content': '',
                      'field': ''
                      }
    return jsonify(result), 201


@outputs.route('/output/visualreportdata', methods=['POST'])
def output_visualreportdata():
    rawdata = RawData()
    val = request.json
    date_selected = val['date_selected']
    machine_code = val['machine_code']
    visualdata = rawdata.getvisualreportdata(machine_code, date_selected)
    return visualdata, 201