from mainapp.models.m_plant import Plant
from flask import Blueprint, jsonify, request, abort
plants = Blueprint('plants', __name__)
import json


@plants.route('/plant/add', methods=['POST'])
def plant_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        plant = Plant()
        plant.plant_code = val['plant_code']
        plant.plant_name = val['plant_name']
        retval = plant.select()
        planteselect = json.loads(retval)
        if len(planteselect) == 1:
            result = plant.update()
        else:
            result = plant.insert()
        return jsonify({'successful': result}), 201


@plants.route('/plant/remove', methods=['POST'])
def plant_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        plant = Plant()
        plant.plant_code = val['plant_code']
        result = plant.remove()
        return jsonify({'successful': result}), 201


@plants.route('/plant/select', methods=['POST'])
def plant_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        plant = Plant()
        plant.plant_code = val['plant_code']
        result = plant.select()
        return result, 201


@plants.route('/plant/selectall', methods=['POST'])
def plant_selectall():
    plant = Plant()
    planteall = plant.selectall()
    return planteall, 201
