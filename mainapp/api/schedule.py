from mainapp.models.m_schedule import Schedule
from flask import Blueprint, jsonify, request, abort
schedules = Blueprint('schedules', __name__)
from mainapp.models.m_machine import Machine
import pandas as pd
import json


@schedules.route('/schedule/add', methods=['POST'])
def schedule_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        schedule = Schedule()
        schedule.machine_code = val['machine_code']
        schedule.numberofdate = val['numberofdate']
        schedule.begininspect = val['begininspect']
        schedule.inspectgroup_code = val['inspectgroup_code']
        retval = schedule.select()
        scheduleselect = json.loads(retval)
        if len(scheduleselect) == 1:
            result = schedule.update()
        else:
            result = schedule.insert()
        return jsonify({'successful': result}), 201


@schedules.route('/schedule/remove', methods=['POST'])
def schedules_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        schedule = Schedule()
        schedule.machine_code = val['machine_code']
        result = schedule.remove()
        return jsonify({'successful': result}), 201


@schedules.route('/schedule/select', methods=['POST'])
def schedule_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        schedule = Schedule()
        schedule.machine_code = val['machine_code']
        result = schedule.select()
        return result, 201


@schedules.route('/schedule/selectall', methods=['POST'])
def schedule_selectall():
    schedule = Schedule()
    scheduleall = schedule.selectall()
    dfschedule = pd.read_json(scheduleall, orient='records')
    machine = Machine()
    machineall = machine.selectall()
    dfmachine = pd.read_json(machineall, orient='records')
    dfschedule = dfschedule.merge(dfmachine, left_on='machine_code', right_on='code')
    dfschedule = dfschedule.to_json(orient='records')
    return dfschedule, 201
