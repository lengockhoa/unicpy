from mainapp.models.m_task import Task
from mainapp.models.m_users import Users
from mainapp.models.m_schedule import Schedule
from flask import Blueprint, jsonify, request, abort
tasks = Blueprint('tasks', __name__)
import datetime
from datetime import datetime
import json


@tasks.route('/task/add', methods=['POST'])
def task_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        task = Task()
        task.machine_code = val['machine_code']
        task.date_inspection = val['date_inspection']
        task.date_last_inspection = val['date_last_inspection']
        task.visual_question = val['visual_question']
        task.vibration = val['vibration']
        task.temperature = val['temperature']
        task.is_checked_visual = val['is_checked_visual']
        task.is_checked_vibration = val['is_checked_vibration']
        task.is_checked_temperature = val['is_checked_temperature']
        task.user_code = val['user_code']
        result = task.insert()
        return jsonify({'successful': result}), 201


@tasks.route('/task/selectbymachinecode', methods=['POST'])
def task_selectbymachinecode():
    if not request.json:
        abort(400)
    else:
        val = request.json
        task = Task()
        task.machine_code = val['machine_code']
        result = task.selectbymachinecode()
        return result, 201


@tasks.route('/task/selectall', methods=['POST'])
def task_selectall():
    task = Task()
    result = task.selectall()
    return result, 201


@tasks.route('/task/selecttoday', methods=['POST'])
def task_selecttoday():
    task = Task()
    result = task.selecttoday()
    return result, 201


@tasks.route('/task/myinspection', methods=['POST'])
def task_myinspection():
    if not request.json:
        abort(400)
    else:
        val = request.json
        user = Users()
        user.username = val['username']
        userselect = user.selectbyusername()
        userselect = json.loads(userselect)
        if len(userselect) == 1:
            task = Task()
            task.user_code = userselect[0]['code']
            fromdate = datetime.now().strftime("%Y-%m-%d")
            todate = datetime.now().strftime("%Y-%m-%d")
            if 'fromdate' in val:
                fromdate = val['fromdate']
            if 'todate' in val:
                todate = val['todate']
            result = task.selectbyusercodebetweendate(fromdate, todate)
            return result, 201
        else:
            retval = []
            return jsonify(retval), 201


@tasks.route('/task/autogenerate', methods=['POST', 'GET'])
def task_autogenerate():
    currentdatestr = datetime.now().strftime("%Y-%m-%d")
    currentdate = datetime.strptime(currentdatestr, "%Y-%m-%d")
    task = Task()
    task.date_answer = currentdatestr
    task.removebydateanswer()
    result = task.selectlastinspect()
    tasklist = json.loads(result)
    for ta in tasklist:
        task.machine_code = ta['machine_code']
        task.date_inspection = ta['date_inspection']
        task.selectlastinspection()
        task.last_inspection = task.selectlastinspection()
        task.inspectgroup_code = ta['inspectgroup_code']
        task.task_type = 'delay'
        task.insert()
    schedule = Schedule()
    result = schedule.selectall()
    schedulelist = json.loads(result)
    for sche in schedulelist:
        datediff = abs((currentdate - datetime.strptime(sche['begininspect'], "%Y-%m-%d")).days)
        is_today = datediff % sche['numberofdate']
        if is_today == 0:
            task.machine_code = sche['machine_code']
            task.date_inspection = currentdatestr
            task.last_inspection = task.selectlastinspection()
            task.inspectgroup_code = sche['inspectgroup_code']
            task.task_type = 'schedule'
            task.insert()
    return result, 201







