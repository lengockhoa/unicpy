from mainapp.models.m_temperature import Temperature
from flask import Blueprint, jsonify, request, abort
temperatures = Blueprint('temperatures', __name__)
from mainapp.models.m_category import Category
from mainapp.models.m_machine import Machine
import pandas as pd
import json


@temperatures.route('/temperature/add', methods=['POST'])
def temperature_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        temperature = Temperature()
        temperature.code = val['code']
        temperature.category_code = val['category_code']
        temperature.standard_alarm_1 = val['standard_alarm_1']
        temperature.standard_alarm_2 = val['standard_alarm_2']
        temperature.uom = val['uom']
        retval = temperature.select()
        temperatureselect = json.loads(retval)
        if len(temperatureselect) == 1:
            result = temperature.update()
        else:
            result = temperature.insert()
        return jsonify({'successful': result}), 201


@temperatures.route('/temperature/select', methods=['POST'])
def temperature_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        temperature = Temperature()
        temperature.code = val['code']
        result = temperature.select()
        return result, 201


@temperatures.route('/temperature/remove', methods=['POST'])
def vibration_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        temperature = Temperature()
        temperature.code = val['code']
        result = temperature.remove()
        return jsonify({'successful': result}), 201


@temperatures.route('/temperature/selectall', methods=['POST'])
def temperature_selectall():
    machine = Machine()
    machineall = machine.selectall()
    dfmachine = pd.read_json(machineall, orient='records')
    category = Category()
    categoryall = category.selectall()
    dfcategory = pd.read_json(categoryall, orient='records')
    temperature = Temperature()
    temperatureall = temperature.selectall()
    dftemperature = pd.read_json(temperatureall, orient='records')

    dftemperature = dftemperature.merge(dfcategory, left_on='category_code', right_on='code')
    dftemperature = dftemperature.merge(dfmachine, left_on='machine_code', right_on='code')
    dftemperature = dftemperature[['code_x', 'code_y', 'machine_code', 'name_x', 'standard_alarm_1', 'standard_alarm_2', 'uom']]
    dftemperature.rename(columns={'code_x': 'code', 'name_x': 'position', 'code_y': 'position_code'}, inplace=True)
    temperatureall = dftemperature.to_json(orient='records')
    return temperatureall, 201
