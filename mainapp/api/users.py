from mainapp.models.m_users import Users
from flask import Blueprint, jsonify, request, abort
users = Blueprint('users', __name__)
import hashlib
import json


@users.route('/user/add', methods=['POST'])
def user_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        user = Users()
        user.code = val['code']
        user.fullname = val['fullname']
        user.username = val['username']
        user.role = val['role']
        user.email = val['email']
        user.plant_code = val['plant_code']
        user.inspectgroup_code = val['inspectgroup_code']
        retval = user.select()
        inspectgroupeselect = json.loads(retval)
        if len(inspectgroupeselect) == 1:
            result = user.update()
        else:
            result = user.insert()
        return jsonify({'successful': result}), 201


@users.route('/user/inactive', methods=['POST'])
def user_inactive():
    if not request.json:
        abort(400)
    else:
        val = request.json
        user = Users()
        user.code = val['code']
        result = user.setinactive()
        return jsonify({'successful': result}), 201


@users.route('/user/select', methods=['POST'])
def user_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        user = Users()
        user.code = val['code']
        result = user.select()
        return result, 201


@users.route('/user/selectall', methods=['POST'])
def user_selectall():
    user = Users()
    result = user.selectall()
    return result, 201


@users.route('/user/login', methods=['POST'])
def user_login():
    if not request.json or 'user_login' not in request.json or 'user_password' not in request.json:
        abort(400)
    else:
        val = request.json
        user = Users()
        user.username = val['user_login']
        user.user_password = hashlib.md5(str(val['user_password']).encode('utf-8')).hexdigest()
        user.os = val.get('os', '')
        user.lang = val.get('lang', '')
        user.token = val.get('token', '')
        user.version = val.get('version', '')
        result = user.login(user.username.lower(), user.user_password)
        listresult = json.loads(result)
        if len(listresult) == 1:
            listresult[0]['successful'] = True
            return jsonify(listresult[0]), 201
        else:
            retval = {'successful': False, 'message': 'Username or Password is incorrect'}
            return jsonify(retval), 201
