from mainapp.models.m_vibration import Vibration
from flask import Blueprint, jsonify, request, abort
vibrations = Blueprint('vibrations', __name__)
from mainapp.models.m_category import Category
from mainapp.models.m_machine import Machine
import pandas as pd
import json


@vibrations.route('/vibration/add', methods=['POST'])
def vibration_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        vibration = Vibration()
        vibration.code = val['code']
        vibration.category_code = val['category_code']
        vibration.standard_alarm_1 = val['standard_alarm_1']
        vibration.standard_alarm_2 = val['standard_alarm_2']
        retval = vibration.select()
        vibrationselect = json.loads(retval)
        if len(vibrationselect) == 1:
            result = vibration.update()
        else:
            result = vibration.insert()
        return jsonify({'successful': result}), 201


@vibrations.route('/vibration/select', methods=['POST'])
def vibration_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        vibration = Vibration()
        vibration.code = val['code']
        result = vibration.select()
        return result, 201


@vibrations.route('/vibration/remove', methods=['POST'])
def vibration_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        vibration = Vibration()
        vibration.code = val['code']
        result = vibration.remove()
        return jsonify({'successful': result}), 201


@vibrations.route('/vibration/selectall', methods=['POST'])
def vibration_selectall():
    machine = Machine()
    machineall = machine.selectall()
    dfmachine = pd.read_json(machineall, orient='records')
    category = Category()
    categoryall = category.selectall()
    dfcategory = pd.read_json(categoryall, orient='records')
    vibration = Vibration()
    vibrationall = vibration.selectall()
    dfvibration = pd.read_json(vibrationall, orient='records')
    dfvibration = dfvibration.merge(dfcategory, left_on='category_code', right_on='code')
    dfvibration = dfvibration.merge(dfmachine, left_on='machine_code', right_on='code')
    dfvibration = dfvibration[['code_x', 'code_y', 'machine_code', 'name_x', 'standard_alarm_1', 'standard_alarm_2']]
    dfvibration.rename(columns={'code_x': 'code', 'name_x': 'position', 'code_y': 'position_code'}, inplace=True)
    vibrationall = dfvibration.to_json(orient='records')
    return vibrationall, 201
