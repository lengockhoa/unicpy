from mainapp.models.m_visual import Visual
from flask import Blueprint, jsonify, request, abort
visuals = Blueprint('visuals', __name__)
import json


@visuals.route('/visual/add', methods=['POST'])
def visual_add():
    if not request.json:
        abort(400)
    else:
        val = request.json
        visual = Visual()
        visual.code = val['code']
        visual.name = val['name']
        visual.name_vn = val['name_vn']
        visual.category_code = val['category_code']
        visual.machine_code = val['machine_code']
        visual.is_dangerous = val['is_dangerous']
        retval = visual.select()
        visualselect = json.loads(retval)
        if len(visualselect) == 1:
            visual.update()
        else:
            visual.insert()
        return jsonify({'successful': True}), 201


@visuals.route('/visual/remove', methods=['POST'])
def visual_remove():
    if not request.json:
        abort(400)
    else:
        val = request.json
        visual = Visual()
        visual.code = val['code']
        result = visual.remove()
        return jsonify({'successful': result}), 201


@visuals.route('/visual/select', methods=['POST'])
def visual_select():
    if not request.json:
        abort(400)
    else:
        val = request.json
        visual = Visual()
        visual.code = val['code']
        result = visual.select()
        return result, 201


@visuals.route('/visual/selectall', methods=['POST'])
def visual_selectall():
    visual = Visual()
    result = visual.selectall()
    return result, 201

