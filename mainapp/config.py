import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
IMAGE_DIR = '/static/image_upload'
IMAGE_MACHINE = '/static/machine_image'
IMAGE_DIR_FULL = ROOT_DIR + '/..' + IMAGE_DIR
IMAGE_MACHINE_FULL = ROOT_DIR + '/..' + IMAGE_MACHINE

BASE_URL = 'http://inspectpro.unicjsc.com'
