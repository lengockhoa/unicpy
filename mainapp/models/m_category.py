from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string


class Category:
    def __init__(self):
        self.code = ''
        self.name = ''
        self.name_vn = ''
        self.machine_code = ''
        self.type = ''
        self.categorygroup_code = ''

    def insert(self):
        query = """
INSERT INTO machine_category(code, name, name_vn, machine_code, type, categorygroup_code) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.code, self.name, self.name_vn, self.machine_code, self.type, self.categorygroup_code)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
            md5string = Md5string()
            md5string.changemanage()
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE machine_category
SET name = '%s',
    name_vn = '%s',
    machine_code = '%s',
    type = '%s',
    categorygroup_code = '%s'
WHERE code = '%s'
""" % (self.name, self.name_vn, self.machine_code, self.type, self.categorygroup_code, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def remove(self):
        query = """
DELETE FROM machine_category
WHERE code = '%s'
""" % self.code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM machine_category
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM machine_category
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbytype(self):
        query = """
SELECT * 
FROM machine_category 
WHERE type LIKE '%s'
""" % ('%' + self.type + '%')
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectbycategorygroup(categorygroup):
        query = """
SELECT * 
FROM machine_category 
WHERE categorygroup_code LIKE '%s'
""" % ('%' + categorygroup + '%')
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
