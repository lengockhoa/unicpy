from mainapp.db import connection, cursor
import pandas as pd


class CategoryGroup:
    def __init__(self):
        self.code = ''
        self.name = ''

    def insert(self):
        query = """
INSERT INTO category_group(code, name) 
VALUES ('%s', '%s')
""" % (self.code, self.name)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE category_group
SET name = '%s'
WHERE code = '%s'
""" % (self.name, self.code)
        cursor.execute(query)
        connection.commit()
        return True

    def remove(self):
        query = """
DELETE FROM category_group
WHERE code = '%s'
""" % self.code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM category_group
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM category_group
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectfrommachinecode(machine_code):
        query = """
SELECT DISTINCT categorygroup_code as code
FROM machine_category
WHERE machine_code = '%s' AND categorygroup_code IS NOT NULL AND categorygroup_code <> ''
""" % machine_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
