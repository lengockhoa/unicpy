from mainapp.db import connection, cursor
import pandas as pd


class DTask:
    def __init__(self):
        self.machine_code = ''
        self.date_inspection = ''
        self.username = ''

    def select(self):
        query = """
SELECT * 
FROM task
WHERE machine_code = '%s' AND date_inspection = '%s' AND username = '%s'
AND is_checked_visual = 'Y' AND is_checked_vibration = 'Y' AND is_checked_temperature = 'Y'
""" % (self.machine_code, self.date_inspection, self.username)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson


class DVisual:
    def __init__(self):
        self.q_code = ''
        self.category_code = ''
        self.machine_code = ''
        self.rs_answer = ''
        self.rs_comment = ''
        self.date_inspection = ''
        self.user_code = ''
        self.date_answer = ''
        self.inspect_type = ''
        self.username = ''

    def insert(self):
        query = """
INSERT INTO data_visual(q_code, category_code, machine_code, 
rs_answer, rs_comment, date_inspection, user_code, date_answer, inspect_type) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.q_code, self.category_code, self.machine_code,self.rs_answer, self.rs_comment,
       self.date_inspection, self.user_code, self.date_answer, self.inspect_type)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE data_visual
SET rs_answer = '%s',
    rs_comment = '%s',
    date_answer = '%s'
WHERE q_code = '%s' AND date_inspection = '%s' AND user_code = '%s' AND category_code = '%s' 
AND machine_code = '%s' AND inspect_type = '%s' 
""" % (self.rs_answer, self.rs_comment, self.date_answer, self.q_code, self.date_inspection, self.user_code,
       self.category_code, self.machine_code, self.inspect_type)
        cursor.execute(query)
        connection.commit()

    def select(self):
        query = """
SELECT * 
FROM data_visual
WHERE machine_code = '%s' AND date_inspection = '%s' AND q_code = '%s' AND category_code = '%s'
""" % (self.machine_code, self.date_inspection, self.q_code, self.category_code)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def removeondemand(self):
        query = """
DELETE FROM data_visual
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = '%s'
""" % (self.machine_code, self.date_inspection, 'on_demand')
        cursor.execute(query)
        connection.commit()
        return True


class DVibration:
    def __init__(self):
        self.q_code = ''
        self.machine_code = ''
        self.rs_h = ''
        self.rs_v = ''
        self.rs_a = ''
        self.date_inspection = ''
        self.date_answer = ''
        self.user_code = ''
        self.inspect_type = ''
        self.username = ''

    def insert(self):
        query = """
INSERT INTO data_vibration(q_code, machine_code, 
rs_h, rs_v, rs_a, date_inspection, date_answer, user_code, inspect_type) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.q_code, self.machine_code, self.rs_h, self.rs_v, self.rs_a,
       self.date_inspection, self.date_answer, self.user_code, self.inspect_type)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE data_vibration
SET rs_h = '%s',
    rs_v = '%s',
    rs_a = '%s',
    date_answer = '%s',
    inspect_type = '%s'
WHERE user_code = '%s' AND q_code = '%s' AND machine_code = '%s'
""" % (self.rs_h, self.rs_v, self.rs_a, self.date_answer, self.inspect_type, self.user_code,
       self.q_code, self.machine_code)
        cursor.execute(query)
        connection.commit()

    def select(self):
        query = """
SELECT * 
FROM data_vibration
WHERE machine_code = '%s' AND date_inspection = '%s' AND q_code = '%s'
""" % (self.machine_code, self.date_inspection, self.q_code)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def removeondemand(self):
        query = """
DELETE FROM data_vibration
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = '%s'
""" % (self.machine_code, self.date_inspection, 'on_demand')
        cursor.execute(query)
        connection.commit()
        return True


class DTemperature:
    def __init__(self):
        self.q_code = ''
        self.machine_code = ''
        self.t_value = ''
        self.date_inspection = ''
        self.date_answer = ''
        self.user_code = ''
        self.inspect_type = ''
        self.username = ''

    def insert(self):
        query = """
INSERT INTO data_temperature(q_code, machine_code, 
t_value, date_inspection, date_answer, user_code, inspect_type) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.q_code, self.machine_code, self.t_value, self.date_inspection, self.date_answer,
       self.user_code, self.inspect_type)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE data_temperature
SET t_value = '%s',
    date_answer = '%s',
    inspect_type = '%s'
WHERE user_code = '%s' AND q_code = '%s' AND machine_code = '%s'
""" % (self.t_value, self.date_answer, self.inspect_type, self.user_code,
       self.q_code, self.machine_code)
        cursor.execute(query)
        connection.commit()

    def select(self):
        query = """
SELECT * 
FROM data_temperature
WHERE machine_code = '%s' AND date_inspection = '%s' AND q_code = '%s'
""" % (self.machine_code, self.date_inspection, self.q_code)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def removeondemand(self):
        query = """
DELETE FROM data_temperature
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = '%s'
""" % (self.machine_code, self.date_inspection, 'on_demand')
        cursor.execute(query)
        connection.commit()
        return True


class DPicture:
    def __init__(self):
        self.q_code = ''
        self.category_code = ''
        self.machine_code = ''
        self.rs_picture = ''
        self.date_inspection = ''
        self.date_answer = ''
        self.user_code = ''
        self.inspect_type = ''

    def insert(self):
        query = """
INSERT INTO picture_visual(q_code, category_code, machine_code, rs_picture, date_inspection, date_answer, user_code, inspect_type) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.q_code, self.category_code, self.machine_code, self.rs_picture, self.date_inspection,
       self.date_answer, self.user_code, self.inspect_type)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE picture_visual
SET category_code = '%s',
    rs_picture = '%s',
    date_answer = '%s'
WHERE q_code = '%s' AND date_inspection = '%s' AND user_code = '%s' AND machine_code = '%s', 
""" % (self.category_code, self.rs_picture, self.date_answer,
       self.q_code, self.date_inspection, self.user_code, self.machine_code)
        cursor.execute(query)
        connection.commit()

    def select(self):
        query = """
SELECT * 
FROM picture_visual
WHERE rs_picture = '%s'
""" % self.rs_picture
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def removeondemand(self):
        query = """
DELETE FROM picture_visual
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = '%s'
""" % (self.machine_code, self.date_inspection, 'on_demand')
        cursor.execute(query)
        connection.commit()
        return True

