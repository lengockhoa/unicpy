from mainapp.db import connection, cursor
import pandas as pd


class InspectGroup:
    def __init__(self):
        self.group_code = ''
        self.group_name = ''

    def insert(self):
        query = """
INSERT INTO inspect_group(group_code, group_name) 
VALUES ('%s', '%s')
""" % (self.group_code, self.group_name)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE inspect_group
SET group_name = '%s'
WHERE group_code = '%s'
""" % (self.group_name, self.group_code)
        cursor.execute(query)
        connection.commit()
        return True

    def remove(self):
        query = """
DELETE FROM inspect_group
WHERE group_code = '%s'
""" % self.group_code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM inspect_group
WHERE group_code = '%s'
""" % self.group_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM inspect_group
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
