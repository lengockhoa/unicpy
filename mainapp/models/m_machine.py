from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string
from mainapp.config import IMAGE_MACHINE, BASE_URL


class Machine:
    def __init__(self):
        self.code = ''
        self.name = ''
        self.name_vn = ''
        self.picture = ''
        self.pos_picture = ''
        self.plant_code = ''
        self.machine_class = ''

    def insert(self):
        query = """
INSERT INTO machine(code, name, name_vn, picture, pos_picture, plant_code, machine_class) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.code, self.name, self.name_vn, self.code+'.jpg', 'pos'+self.code+'.jpg', self.plant_code, self.machine_class)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
            md5string = Md5string()
            md5string.changemanage()
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE machine
SET name = '%s',
    name_vn = '%s',
    picture = '%s',
    pos_picture = '%s',
    plant_code = '%s',
    machine_class = '%s'
WHERE code = '%s'
""" % (self.name, self.name_vn, self.picture, self.pos_picture, self.plant_code, self.machine_class, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def updatepicture(self):
        query = """
UPDATE machine
SET picture = '%s'
WHERE code = '%s'
""" % (self.picture, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def updatposepicture(self):
        query = """
UPDATE machine
SET pos_picture = '%s'
WHERE code = '%s'
""" % (self.pos_picture, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def select(self):
        query = """
SELECT *
FROM machine
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM machine
"""
        pdres = pd.read_sql(query, con=connection)
        pdres['picture_full'] = BASE_URL + IMAGE_MACHINE + '/' + pdres['picture']
        pdres['pos_picture_full'] = BASE_URL + IMAGE_MACHINE + '/' + pdres['pos_picture']
        valuejson = pdres.to_json(orient='records')
        return valuejson
