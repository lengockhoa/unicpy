from mainapp.db import connection, cursor
import pandas as pd
import hashlib


class Md5string:
    def __init__(self):
        self.checkstring = ''
        self.code = 'MASTER'

    def updatemaster(self):
        query = """
UPDATE checkstring
SET checkstring = '%s'
WHERE code = 'MASTER'
""" % self.checkstring
        cursor.execute(query)
        connection.commit()

    @staticmethod
    def selectmaster():
        query = """
SELECT *
FROM checkstring
WHERE code = 'MASTER'
"""
        pdres = pd.read_sql(query, con=connection)
        pdres = pdres[['checkstring']]
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def changemanage(self):
        query = """
SELECT *
FROM machine
"""
        pdmachine = pd.read_sql(query, con=connection)
        strmachine = pdmachine.to_string()
        query = """
SELECT *
FROM plant
"""
        pdplant = pd.read_sql(query, con=connection)
        strplant = pdplant.to_string()
        query = """
SELECT *
FROM temperature
"""
        pdtemperature = pd.read_sql(query, con=connection)
        strtemperature = pdtemperature.to_string()
        query = """
SELECT *
FROM vibration
"""
        pdvibration = pd.read_sql(query, con=connection)
        strvibration = pdvibration.to_string()
        query = """
SELECT *
FROM visual
"""
        pdvisual = pd.read_sql(query, con=connection)
        strvisual = pdvisual.to_string()
        query = """
SELECT *
FROM machine_category
"""
        pdcategory = pd.read_sql(query, con=connection)
        strcategory = pdcategory.to_string()
        totalstring = strmachine + strplant + strtemperature + strvibration + strvisual + strcategory
        self.checkstring = hashlib.md5(totalstring.encode('utf-8')).hexdigest()
        self.updatemaster()





