from mainapp.db import connection, cursor
from mainapp.config import IMAGE_DIR, BASE_URL
import pandas as pd
import numpy as np
import json
import datetime
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class DMachineInspect:
    def __init__(self):
        self.date_inspection = ''
        self.machine_code = ''

    def selectvisualbydate(self):
        query = """
SELECT v.code, v.name, v.name_vn, d.machine_code, c.code as category_code, c.name as category_name, c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous
FROM data_visual d
LEFT JOIN visual v ON d.q_code = v.code
LEFT JOIN machine_category c ON d.category_code = c.code 
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY v.code
""" % (self.machine_code, self.date_inspection)
        dfvisual = pd.read_sql(query, con=connection)
        query = """
SELECT * 
FROM picture_visual
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = 'on_demand'
""" % (self.machine_code, self.date_inspection)
        dfpicture = pd.read_sql(query, con=connection)
        if len(dfvisual) == 0:
            query = """
            SELECT v.code, v.name, v.name_vn, d.machine_code, c.code as category_code, c.name as category_name, c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous
            FROM data_visual d
            LEFT JOIN visual v ON d.q_code = v.code
            LEFT JOIN machine_category c ON d.category_code = c.code 
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY v.code
            """ % (self.machine_code, self.date_inspection)
            dfvisual = pd.read_sql(query, con=connection)
            query = """
            SELECT * 
            FROM picture_visual
            WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = 'schedule'
            """ % (self.machine_code, self.date_inspection)
            dfpicture = pd.read_sql(query, con=connection)
        visualjson = dfvisual.to_json(orient='records')
        visualarr = json.loads(visualjson)
        picturejson = dfpicture.to_json(orient='records')
        picturearr = json.loads(picturejson)

        for vis in visualarr:
            vis['rs_picture'] = []
            for pic in picturearr:
                if vis['code'] == pic['q_code'] and vis['category_code'] == pic['category_code']:
                    vis['rs_picture'].append(BASE_URL + IMAGE_DIR + '/' + pic['rs_picture'])
        return json.dumps(visualarr)

    def selectvisualbydatedanger(self):
        query = """
SELECT v.code, v.name, v.name_vn, d.machine_code, c.code as category_code, c.name as category_name, c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous
FROM data_visual d
LEFT JOIN visual v ON d.q_code = v.code
LEFT JOIN machine_category c ON d.category_code = c.code 
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY v.code
""" % (self.machine_code, self.date_inspection)
        dfvisual = pd.read_sql(query, con=connection)
        query = """
SELECT * 
FROM picture_visual
WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = 'on_demand'
""" % (self.machine_code, self.date_inspection)
        dfpicture = pd.read_sql(query, con=connection)
        if len(dfvisual) == 0:
            query = """
            SELECT v.code, v.name, v.name_vn, d.machine_code, c.code as category_code, c.name as category_name, c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous
            FROM data_visual d
            LEFT JOIN visual v ON d.q_code = v.code
            LEFT JOIN machine_category c ON d.category_code = c.code 
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY v.code
            """ % (self.machine_code, self.date_inspection)
            dfvisual = pd.read_sql(query, con=connection)
            query = """
            SELECT * 
            FROM picture_visual
            WHERE machine_code = '%s' AND date_inspection = '%s' AND inspect_type = 'schedule'
            """ % (self.machine_code, self.date_inspection)
            dfpicture = pd.read_sql(query, con=connection)
        visualjson = dfvisual.to_json(orient='records')
        visualarr = json.loads(visualjson)
        picturejson = dfpicture.to_json(orient='records')
        picturearr = json.loads(picturejson)

        for vis in visualarr:
            vis['rs_picture'] = []
            for pic in picturearr:
                if vis['code'] == pic['q_code'] and vis['category_code'] == pic['category_code']:
                    vis['rs_picture'].append(BASE_URL + IMAGE_DIR + '/' + pic['rs_picture'])
        pdres = pd.read_json(json.dumps(visualarr))
        conditions = [
            (pdres['rs_answer'] == 'N'),
            (pdres['rs_answer'] == 'Y'),
        ]
        choices = ['A', 'C']
        pdres['result'] = np.select(conditions, choices, default='A')
        pdres = pdres.sort_values(by='result', ascending=False)
        first_value = pdres['result'].values[0]
        if first_value == 'C':
            pdres = pdres[(pdres['result'] == 'C')]
        if first_value == 'A':
            pdres = pdres[(pdres['result'] == 'None')]
        pdres = pdres.drop(columns=['result'])
        visualjson = pdres.to_json(orient='records')
        return visualjson

    def selectvibrationbydate(self):
        query = """
SELECT t.code, c.name as name, c.name_vn as name_vn, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
FROM data_vibration d
LEFT JOIN vibration t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY t.code
""" % (self.machine_code, self.date_inspection)
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, c.name as name, c.name_vn as name_vn, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
            FROM data_vibration d
            LEFT JOIN vibration t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY t.code
            """ % (self.machine_code, self.date_inspection)
            pdres = pd.read_sql(query, con=connection)
        vibrationjson = pdres.to_json(orient='records')
        return vibrationjson

    def selectvibrationbydatedanger(self):
        query = """
SELECT t.code, c.name as name, c.name_vn as name_vn, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
FROM data_vibration d
LEFT JOIN vibration t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY t.code
""" % (self.machine_code, self.date_inspection)
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, c.name as name, c.name_vn as name_vn, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
            FROM data_vibration d
            LEFT JOIN vibration t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY t.code
            """ % (self.machine_code, self.date_inspection)
            pdres = pd.read_sql(query, con=connection)
        pdres['standard_alarm_1'] = pdres['standard_alarm_1'].fillna(99999999)
        pdres['standard_alarm_2'] = pdres['standard_alarm_2'].fillna(99999999)
        conditions = [
            (pdres['rs_h'] <= pdres['standard_alarm_1']) & (pdres['rs_v'] <= pdres['standard_alarm_1']) & (pdres['rs_a'] <= pdres['standard_alarm_1']),
            (pdres['rs_h'] > pdres['standard_alarm_2']) | (pdres['rs_v'] > pdres['standard_alarm_2']) | (pdres['rs_a'] > pdres['standard_alarm_2']),
            ((pdres['rs_h'] > pdres['standard_alarm_1']) & (pdres['rs_h'] <= pdres['standard_alarm_2'])) |
            ((pdres['rs_v'] > pdres['standard_alarm_1']) & (pdres['rs_v'] <= pdres['standard_alarm_2'])) |
            ((pdres['rs_a'] > pdres['standard_alarm_1']) & (pdres['rs_a'] <= pdres['standard_alarm_2'])),
        ]
        choices = ['A', 'C', 'B']
        pdres['result'] = np.select(conditions, choices, default='A')
        pdres = pdres.sort_values(by='result', ascending=False)
        first_value = pdres['result'].values[0]
        if first_value == 'C':
            pdres = pdres[(pdres['result'] == 'C') | (pdres['result'] == 'B')]
        if first_value == 'B':
            pdres = pdres[(pdres['result'] == 'B')]
        if first_value == 'A':
            pdres = pdres[(pdres['result'] == 'None')]
        pdres = pdres.drop(columns=['result'])
        vibrationjson = pdres.to_json(orient='records')
        return vibrationjson

    def selecttemperaturebydate(self):
        query = """
SELECT t.code, c.name as name, c.name_vn as name_vn, d.t_value, d.t_value as rs_h, t.standard_alarm_1, t.standard_alarm_2
FROM data_temperature d
LEFT JOIN temperature t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY t.code
""" % (self.machine_code, self.date_inspection)
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, c.name as name, c.name_vn as name_vn, d.t_value, d.t_value as rs_h, t.standard_alarm_1, t.standard_alarm_2
            FROM data_temperature d
            LEFT JOIN temperature t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY t.code
            """ % (self.machine_code, self.date_inspection)
            pdres = pd.read_sql(query, con=connection)
        temperaturejson = pdres.to_json(orient='records')
        return temperaturejson

    def selecttemperaturebydatedanger(self):
        query = """
SELECT t.code, c.name as name, c.name_vn as name_vn, d.t_value, d.t_value as rs_h, t.standard_alarm_1, t.standard_alarm_2
FROM data_temperature d
LEFT JOIN temperature t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY t.code
""" % (self.machine_code, self.date_inspection)
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, c.name as name, c.name_vn as name_vn, d.t_value, d.t_value as rs_h, t.standard_alarm_1, t.standard_alarm_2
            FROM data_temperature d
            LEFT JOIN temperature t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.machine_code = '%s' AND d.date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY t.code
            """ % (self.machine_code, self.date_inspection)
            pdres = pd.read_sql(query, con=connection)
        pdres['standard_alarm_1'] = pdres['standard_alarm_1'].fillna(99999999)
        pdres['standard_alarm_2'] = pdres['standard_alarm_2'].fillna(99999999)
        conditions = [
            (pdres['t_value'] <= pdres['standard_alarm_1']),
            (pdres['t_value'] > pdres['standard_alarm_2']),
            ((pdres['t_value'] > pdres['standard_alarm_1']) & (pdres['t_value'] <= pdres['standard_alarm_2'])),
        ]
        choices = ['A', 'C', 'B']
        pdres['result'] = np.select(conditions, choices, default='A')
        pdres = pdres.sort_values(by='result', ascending=False)
        first_value = pdres['result'].values[0]
        if first_value == 'C':
            pdres = pdres[(pdres['result'] == 'C') | (pdres['result'] == 'B')]
        if first_value == 'B':
            pdres = pdres[(pdres['result'] == 'B')]
        if first_value == 'A':
            pdres = pdres[(pdres['result'] == 'None')]
        pdres = pdres.drop(columns=['result'])
        temperaturejson = pdres.to_json(orient='records')
        return temperaturejson

    @staticmethod
    def calculatevisualtask(date_inspection):
        result = []
        query = """
SELECT machine_code, rs_answer
FROM data_visual
WHERE date_inspection = '%s' AND inspect_type = 'on_demand'
ORDER BY rs_answer
""" % date_inspection
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT machine_code, rs_answer
            FROM data_visual
            WHERE date_inspection = '%s' AND inspect_type = 'schedule'
            ORDER BY rs_answer
            """ % date_inspection
            pdres = pd.read_sql(query, con=connection)
        grade = 'A'
        for res in pdres.itertuples():
            if res.rs_answer == 'N' and grade == 'A':
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_visual': 'A'})
            if res.rs_answer == 'Y':
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_visual': 'C'})
                grade = 'C'
        return result

    @staticmethod
    def calculatevibrationtask(date_inspection):
        result = []
        query = """
SELECT t.code, d.machine_code, c.name as position, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
FROM data_vibration d
LEFT JOIN vibration t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection = '%s' AND inspect_type = 'on_demand'
""" % date_inspection
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, d.machine_code, c.name as position, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2
            FROM data_vibration d
            LEFT JOIN vibration t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.date_inspection = '%s' AND inspect_type = 'schedule'
            """ % date_inspection
            pdres = pd.read_sql(query, con=connection)
        grade = 'A'
        for res in pdres.itertuples():
            newvala = {'machine_code': res.machine_code, 'grade_vibration': 'A'}
            newvalb = {'machine_code': res.machine_code, 'grade_vibration': 'B'}
            newvalc = {'machine_code': res.machine_code, 'grade_vibration': 'C'}
            if newvala not in result and newvalb not in result and newvalc not in result:
                result.append({'machine_code': res.machine_code, 'grade_vibration': 'A'})
            if res.rs_h > res.standard_alarm_1 or res.rs_v > res.standard_alarm_1 or res.rs_a > res.standard_alarm_1 \
                    and (grade == 'A'):
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_vibration': 'B'})
                grade = 'B'
            if res.rs_h > res.standard_alarm_2 or res.rs_v > res.standard_alarm_2 or res.rs_a > res.standard_alarm_2 \
                    and (grade == 'A' or grade == 'B'):
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_vibration': 'C'})
        return result

    @staticmethod
    def calculatetemperaturetask(date_inspection):
        result = []
        query = """
SELECT t.code, d.machine_code, c.name as position, d.t_value, t.standard_alarm_1, t.standard_alarm_2
FROM data_temperature d
LEFT JOIN temperature t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection = '%s' and inspect_type = 'on_demand'
""" % date_inspection
        pdres = pd.read_sql(query, con=connection)
        if len(pdres) == 0:
            query = """
            SELECT t.code, d.machine_code, c.name as position, d.t_value, t.standard_alarm_1, t.standard_alarm_2
            FROM data_temperature d
            LEFT JOIN temperature t ON t.code = d.q_code
            LEFT JOIN machine_category c ON t.category_code = c.code
            WHERE d.date_inspection = '%s' and inspect_type = 'schedule'
            """ % date_inspection
            pdres = pd.read_sql(query, con=connection)
        pdres['standard_alarm_1'] = pdres['standard_alarm_1'].fillna(99999999)
        pdres['standard_alarm_2'] = pdres['standard_alarm_2'].fillna(99999999)
        grade = 'A'
        for res in pdres.itertuples():
            newvala = {'machine_code': res.machine_code, 'grade_temperature': 'A'}
            newvalb = {'machine_code': res.machine_code, 'grade_temperature': 'B'}
            newvalc = {'machine_code': res.machine_code, 'grade_temperature': 'C'}
            if newvala not in result and newvalb not in result and newvalc not in result:
                result.append({'machine_code': res.machine_code, 'grade_temperature': 'A'})
            if res.t_value > res.standard_alarm_1 and grade == 'A':
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_temperature': 'B'})
                grade = 'B'
            if res.t_value > res.standard_alarm_2 and (grade == 'A' or grade == 'B'):
                result = list(filter(lambda x: x['machine_code'] != res.machine_code, result))
                result.append({'machine_code': res.machine_code, 'grade_temperature': 'C'})
        return result


class RawData:
    def __init__(self):
        self.username = ''

    @staticmethod
    def gettemperaturedata(from_date, to_date):
        query = """
SELECT t.code, d.machine_code, c.name as position, d.t_value, 
t.standard_alarm_1, t.standard_alarm_2, d.date_inspection, d.date_answer, d.inspect_type
FROM data_temperature d
LEFT JOIN temperature t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection BETWEEN '%s' AND '%s'
""" % (from_date, to_date)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def getvibrationdata(from_date, to_date):
        query = """
SELECT t.code, d.machine_code, c.name as position, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2, d.inspect_type, d.date_inspection
FROM data_vibration d
LEFT JOIN vibration t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection BETWEEN '%s' AND '%s'
""" % (from_date, to_date)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def getvisualdata(from_date, to_date):
        query = """
SELECT v.code, v.name, v.name_vn, d.machine_code, d.date_inspection, c.code as category_code, c.name as category_name, 
c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous, d.inspect_type
FROM data_visual d
LEFT JOIN visual v ON d.q_code = v.code
LEFT JOIN machine_category c ON d.category_code = c.code
WHERE d.date_inspection BETWEEN '%s' AND '%s'
""" % (from_date, to_date)
        dfvisual = pd.read_sql(query, con=connection)
        query = """
SELECT * 
FROM picture_visual
WHERE date_inspection BETWEEN '%s' AND '%s'
""" % (from_date, to_date)
        dfpicture = pd.read_sql(query, con=connection)
        visualjson = dfvisual.to_json(orient='records')
        visualarr = json.loads(visualjson)
        picturejson = dfpicture.to_json(orient='records')
        picturearr = json.loads(picturejson)
        for vis in visualarr:
            vis['rs_picture'] = []
            for pic in picturearr:
                if vis['code'] == pic['q_code'] and \
                        vis['category_code'] == pic['category_code'] and \
                        vis['machine_code'] == pic['machine_code'] and \
                        vis['date_inspection'] == pic['date_inspection'] and \
                        vis['inspect_type'] == pic['inspect_type']:
                    vis['rs_picture'].append(BASE_URL + IMAGE_DIR + '/' + pic['rs_picture'])
        return json.dumps(visualarr)

    @staticmethod
    def getvisualreportdata(machine_code, date_selected):
        query = """
SELECT v.code, v.name, v.name_vn, d.machine_code, c.code as category_code, c.name as category_name,
c.name_vn as category_name_vn, d.rs_answer, d.rs_comment, v.is_dangerous, d.inspect_type, d.date_inspection,
u.fullname
FROM data_visual d
LEFT JOIN visual v ON d.q_code = v.code
LEFT JOIN machine_category c ON d.category_code = c.code
LEFT JOIN users u ON u.code = d.user_code
WHERE d.machine_code = '%s'
AND  d.date_inspection IN (SELECT MAX(date_inspection) FROM data_visual v WHERE v.date_inspection <= '%s' AND v.machine_code = '%s')
""" % (machine_code, date_selected, machine_code)
        dfvisual = pd.read_sql(query, con=connection)
        visualjson = dfvisual.to_json(orient='records')
        return visualjson

    @staticmethod
    def getdataforchartvibration(date_selected, machine_code, category_code):
        from_date = datetime.strptime(date_selected, "%Y-%m-%d") - relativedelta(months=6)
        from_date_str = from_date.strftime("%Y-%m-%d")
        query = """
SELECT d.machine_code, c.name as position, d.rs_h, d.rs_v, d.rs_a, t.standard_alarm_1, t.standard_alarm_2,d.date_inspection,d.inspect_type
FROM data_vibration d
LEFT JOIN vibration t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection BETWEEN '%s' AND '%s' AND d.machine_code = '%s' and c.code = '%s'
ORDER BY d.inspect_type DESC, d.date_inspection ASC
""" % (from_date_str, date_selected, machine_code, category_code)
        pdres = pd.read_sql(query, con=connection)
        pdres = pdres.drop_duplicates(subset=['machine_code', 'position', 'date_inspection'], keep="last")
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def getdataforcharttemperature(date_selected, machine_code, category_code):
        from_date = datetime.strptime(date_selected, "%Y-%m-%d") - relativedelta(months=6)
        from_date_str = from_date.strftime("%Y-%m-%d")
        query = """
SELECT t.code, d.machine_code, c.name as position, d.t_value, 
t.standard_alarm_1, t.standard_alarm_2, d.date_inspection, d.inspect_type
FROM data_temperature d
LEFT JOIN temperature t ON t.code = d.q_code
LEFT JOIN machine_category c ON t.category_code = c.code
WHERE d.date_inspection BETWEEN '%s' AND '%s' AND d.machine_code = '%s' and c.code = '%s'
ORDER BY d.inspect_type DESC, d.date_inspection ASC 
""" % (from_date_str, date_selected, machine_code, category_code)
        pdres = pd.read_sql(query, con=connection)
        pdres = pdres.drop_duplicates(subset=['machine_code', 'position', 'date_inspection'], keep="last")
        valuejson = pdres.to_json(orient='records')
        return valuejson
