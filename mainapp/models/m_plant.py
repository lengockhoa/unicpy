from mainapp.db import connection, cursor
import pandas as pd


class Plant:
    def __init__(self):
        self.plant_code = ''
        self.plant_name = ''

    def insert(self):
        query = """
INSERT INTO plant(plant_code, plant_name) 
VALUES ('%s', '%s')
""" % (self.plant_code, self.plant_name)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE plant
SET plant_name = '%s',
WHERE plant_code = '%s'
""" % (self.plant_name, self.plant_code)
        cursor.execute(query)
        connection.commit()
        return True

    def remove(self):
        query = """
DELETE FROM plant
WHERE plant_code = '%s'
""" % self.plant_code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM plant
WHERE plant_code = '%s'
""" % self.plant_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM plant
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
