from mainapp.db import connection, cursor
import pandas as pd


class Schedule:
    def __init__(self):
        self.machine_code = ''
        self.numberofdate = ''
        self.begininspect = ''
        self.inspectgroup_code = ''

    def insert(self):
        query = """
INSERT INTO schedule(machine_code, numberofdate, begininspect, inspectgroup_code) 
VALUES ('%s', '%s', '%s', '%s')
""" % (self.machine_code, self.numberofdate, self.begininspect, self.inspectgroup_code)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE schedule
SET numberofdate = '%s',
    begininspect = '%s',
    inspectgroup_code = '%s'
WHERE machine_code = '%s'
""" % (self.numberofdate, self.begininspect, self.inspectgroup_code, self.machine_code)
        cursor.execute(query)
        connection.commit()
        return True

    def remove(self):
        query = """
DELETE FROM schedule
WHERE machine_code = '%s'
""" % self.machine_code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM schedule
WHERE machine_code = '%s'
""" % self.machine_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbyusercode(self, usercode):
        query = """
SELECT *
FROM schedule
WHERE inspectgroup_code IN (SELECT users.inspectgroup_code from users WHERE code = '%s') 
""" % usercode
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM schedule
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
