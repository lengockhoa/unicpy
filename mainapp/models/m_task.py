from mainapp.db import connection, cursor
import pandas as pd


class Task:
    def __init__(self):
        self.machine_code = ''
        self.date_inspection = ''
        self.last_inspection = ''
        self.is_checked_visual = 'N'
        self.is_checked_vibration = 'N'
        self.is_checked_temperature = 'N'
        self.user_code = ''
        self.inspectgroup_code = ''
        self.task_type = ''
        self.date_answer = ''
        self.not_check = ''
        self.on_demand = ''
        self.reason = ''
        self.sync_time = ''
        self.username = ''

    def insert(self):
        query = """
INSERT INTO task(machine_code, date_inspection, last_inspection, 
is_checked_visual, is_checked_vibration, is_checked_temperature, inspectgroup_code, task_type, 
date_answer, not_check, on_demand, reason, sync_time, username) 
VALUES ('%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.machine_code, self.date_inspection, self.last_inspection,self.is_checked_visual,
       self.is_checked_vibration, self.is_checked_temperature, self.inspectgroup_code, self.task_type,
       self.date_answer, self.not_check, self.on_demand, self.reason, self.sync_time, self.username)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE task
SET date_inspection = '%s', 
    last_inspection = '%s',
    is_checked_visual = '%s',
    is_checked_vibration = '%s',
    is_checked_temperature = '%s',
    inspectgroup_code = '%s',
    task_type = '%s',
    date_answer = '%s',
    username = '%s'
WHERE machine_code = '%s'
""" % (self.date_inspection, self.last_inspection, self.is_checked_visual, self.is_checked_vibration,
       self.is_checked_temperature, self.inspectgroup_code, self.task_type, self.date_answer, self.username,
       self.machine_code)
        cursor.execute(query)
        connection.commit()

    def removebydateanswer(self):
        query = """
DELETE FROM task
WHERE date_answer = '%s'
""" % self.date_answer
        cursor.execute(query)
        connection.commit()

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM task
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selecttoday():
        query = """
SELECT *
FROM task 
WHERE date_answer = TO_CHAR(CURRENT_DATE, 'YYYY-MM-DD');
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbymachineandinspectiondate(self):
        query = """
SELECT *
FROM task
WHERE machine_code = '%s' AND date_inspection = '%s' AND date_answer = '%s'
""" % (self.machine_code, self.date_inspection, self.date_answer)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbymachinecode(self):
        query = """
SELECT *
FROM task
WHERE machine_code = '%s'
""" % self.machine_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbyusercode(self):
        query = """
SELECT *
FROM task
WHERE inspectgroup_code IN (SELECT inspectgroup_code FROM users WHERE code = '%s')
""" % self.user_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbyusercodeanddate(self):
        query = """
SELECT *
FROM task
WHERE date_answer = '%s' AND inspectgroup_code IN (SELECT inspectgroup_code FROM users WHERE code = '%s')
""" % (self.user_code, self.date_answer)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbyusercodebetweendate(self, fromdate, todate):
        query = """
SELECT *
FROM task
WHERE date_answer BETWEEN '%s' AND '%s' AND inspectgroup_code IN (SELECT inspectgroup_code FROM users WHERE code = '%s')
AND (is_checked_visual = 'N' AND is_checked_temperature = 'N' AND is_checked_vibration = 'N')
""" % (fromdate, todate, self.user_code)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectlastinspect():
        query = """
SELECT *
FROM task
WHERE (machine_code, date_answer) IN (
    SELECT machine_code, max(date_answer) FROM task GROUP BY machine_code
)
"""
        dftask = pd.read_sql(query, con=connection)
        dftask['date_inspection'] = dftask['date_inspection'].astype(str)
        dftask['last_inspection'] = dftask['last_inspection'].astype(str)
        dftask['date_answer'] = dftask['date_answer'].astype(str)
        rowindex = []
        for row in dftask.itertuples():
            if row.is_checked_visual == 'Y' and row.is_checked_vibration == 'Y' and row.is_checked_temperature == 'Y':
                rowindex.append(getattr(row, 'Index'))
        dftask = dftask.drop(rowindex, axis='rows')

        valuejson = dftask.to_json(orient='records')
        return valuejson

    def selectlastinspection(self):
        query = """
SELECT *
FROM task
WHERE is_checked_visual = 'Y' AND is_checked_temperature = 'Y' AND is_checked_vibration = 'Y' AND machine_code = '%s'
LIMIT 1
""" % self.machine_code
        pdres = pd.read_sql(query, con=connection)
        pdres['date_inspection'] = pdres['date_inspection'].astype(str)
        pdres['last_inspection'] = pdres['last_inspection'].astype(str)
        dateinspect = '' if len(pdres) == 0 else pdres.loc[pdres.index[0], 'date_answer']
        return dateinspect

    def updatebyuser(self):
        query = """
UPDATE task
SET is_checked_visual = '%s',
    is_checked_vibration = '%s',
    is_checked_temperature = '%s',
    inspectgroup_code = '%s',
    task_type = '%s',
    not_check = '%s',
    on_demand = '%s',
    reason = '%s',
    sync_time = '%s',
    username = '%s'
WHERE machine_code = '%s' AND date_inspection = '%s' AND date_answer = '%s' 
""" % (self.is_checked_visual, self.is_checked_vibration, self.is_checked_temperature,
       self.inspectgroup_code, self.task_type, self.not_check,
       self.on_demand, self.reason, self.sync_time, self.username,
       self.machine_code, self.date_inspection, self.date_answer)
        cursor.execute(query)
        connection.commit()
        return True
