from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string


class Temperature:
    def __init__(self):
        self.code = ''
        self.standard_alarm_1 = ''
        self.standard_alarm_2 = ''
        self.category_code = ''
        self.uom = ''

    def insert(self):
        query = """
INSERT INTO temperature(code, standard_alarm_1, standard_alarm_2, category_code, uom) 
VALUES ('%s', '%s', '%s', '%s','%s')
""" % (self.code, self.standard_alarm_1, self.standard_alarm_2, self.category_code, self.uom)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
            md5string = Md5string()
            md5string.changemanage()
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE temperature
SET standard_alarm_1 = '%s', 
    standard_alarm_2 = '%s',
    category_code = '%s',
    uom = '%s'
WHERE code = '%s'
""" % (self.standard_alarm_1, self.standard_alarm_2, self.category_code, self.uom, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def remove(self):
        query = """
DELETE FROM temperature
WHERE code = '%s'
""" % self.code
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def select(self):
        query = """
SELECT *
FROM temperature
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM temperature
ORDER BY code
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
