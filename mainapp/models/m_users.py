from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string


class Users:
    def __init__(self):
        self.code = ''
        self.fullname = ''
        self.username = ''
        self.password = '202cb962ac59075b964b07152d234b70'
        self.role = 'INSPECTOR'
        self.plant_code = ''
        self.email = ''
        self.is_active = '1'
        self.inspectgroup_code = ''

    def insert(self):
        query = """
INSERT INTO users(code, fullname, username, password, role, email, plant_code, inspectgroup_code, is_active) 
VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
""" % (self.code, self.fullname, self.username, self.password, self.role, self.email,
       self.plant_code, self.inspectgroup_code, self.is_active)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE users
SET fullname = '%s',
    username = '%s',
    password = '%s',
    role = '%s',
    email = '%s',
    plant_code = '%s',
    inspectgroup_code = '%s'
WHERE code = '%s'
""" % (self.fullname, self.username, self.password, self.role, self.email, self.plant_code,
       self.inspectgroup_code, self.code)
        cursor.execute(query)
        connection.commit()
        return True

    def setinactive(self):
        query = """
UPDATE users
SET is_active = '0'
WHERE code = '%s'
""" % self.code
        cursor.execute(query)
        connection.commit()
        return True

    def select(self):
        query = """
SELECT *
FROM users
WHERE code = '%s' AND is_active = '1'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    def selectbyusername(self):
        query = """
SELECT *
FROM users
WHERE username = '%s' AND is_active = '1'
""" % self.username
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM users
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectactive():
        query = """
SELECT *
FROM users
WHERE is_active = '1'
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def login(username, password):
        query = """
SELECT code, fullname, username, role, email, plant_code
FROM users
WHERE username = '%s' AND password = '%s' AND is_active = '1'
LIMIT 1
""" % (username, password)
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
