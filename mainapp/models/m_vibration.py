from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string


class Vibration:
    def __init__(self):
        self.code = ''
        self.standard_alarm_1 = ''
        self.standard_alarm_2 = ''
        self.category_code = ''

    def insert(self):
        query = """
INSERT INTO vibration(code, standard_alarm_1, standard_alarm_2, category_code) 
VALUES ('%s', '%s', '%s', '%s')
""" % (self.code, self.standard_alarm_1, self.standard_alarm_2, self.category_code)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
            md5string = Md5string()
            md5string.changemanage()
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE vibration
SET standard_alarm_1 = '%s', 
    standard_alarm_2 = '%s',
    category_code = '%s'
WHERE code = '%s'
""" % (self.standard_alarm_1, self.standard_alarm_2, self.category_code, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def remove(self):
        query = """
DELETE FROM vibration
WHERE code = '%s'
""" % self.code
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def select(self):
        query = """
SELECT *
FROM vibration
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM vibration
ORDER BY code
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
