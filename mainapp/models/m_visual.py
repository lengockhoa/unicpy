from mainapp.db import connection, cursor
import pandas as pd
from mainapp.models.m_md5string import Md5string


class Visual:
    def __init__(self):
        self.code = ''
        self.name = ''
        self.name_vn = ''
        self.category_code = ''
        self.machine_code = ''
        self.is_dangerous = 'Y'

    def insert(self):
        query = """
INSERT INTO visual(code, name, name_vn, category_code, machine_code, is_dangerous) 
VALUES ('%s', '%s', '%s', '%s','%s','%s')
""" % (self.code, self.name, self.name_vn, self.category_code, self.machine_code, self.is_dangerous)
        try:
            cursor.execute(query)
            connection.commit()
            result = True
            md5string = Md5string()
            md5string.changemanage()
        except:
            connection.rollback()
            result = False
        return result

    def update(self):
        query = """
UPDATE visual
SET name = '%s', 
    name_vn = '%s',
    category_code = '%s',
    machine_code = '%s',
    is_dangerous = '%s'
WHERE code = '%s'
""" % (self.name, self.name_vn, self.category_code, self.machine_code, self.is_dangerous, self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()
        return True

    def remove(self):
        query = """
DELETE FROM visual
WHERE code = '%s'
""" % (self.code)
        cursor.execute(query)
        connection.commit()
        md5string = Md5string()
        md5string.changemanage()

    def select(self):
        query = """
SELECT *
FROM visual
WHERE code = '%s'
""" % self.code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson

    @staticmethod
    def selectall():
        query = """
SELECT *
FROM visual
ORDER BY code
"""
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
