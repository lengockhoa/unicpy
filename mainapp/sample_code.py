from flask import Blueprint, jsonify, request, abort
sample = Blueprint('sample', __name__)
#import các thư viện cần thiết
from werkzeug.utils import secure_filename
import os
from mainapp.config import IMAGE_DIR_FULL
from crontab import CronTab


@sample.route('/kiemtra', methods=['POST'])
def kiemtra():
    if not request.json:
        abort(400)
    else:
        return jsonify({'task': request.json}), 201


@sample.route('/sendfile', methods=['POST'])
def sendfile():
    if 'file' not in request.files:
        abort(400)
    file = request.files['file']
    if file.filename == '':
        abort(400)
    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(IMAGE_DIR_FULL, filename))
        return jsonify({'task': request.json}), 201


@sample.route('/crontab', methods=['POST'])
def crontab():
    my_cron = CronTab(user='lenk')
    for job in my_cron:
        print(job)
        my_cron.remove(job)
    job = my_cron.new(command='wget http://inspectpro.unicjsc.com/task/autogenerate', comment='task_autogenerate')
    # set 1 giờ 30 phút hàng ngày
    job.hour.on(1)
    job.minute.on(30)

    my_cron.write()
    return jsonify({'mission': "completed"}), 201
