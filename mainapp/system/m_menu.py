from mainapp.db import connection
import pandas as pd


class Menu:
    def __init__(self):
        self.user_code = ''
        self.menu_code = ''

    def getlistactive(self):
        query = """
SELECT *
FROM menu
WHERE code IN (SELECT menu_code FROM menu_permission WHERE user_code = '%s')
ORDER BY sequence
""" % self.user_code
        pdres = pd.read_sql(query, con=connection)
        valuejson = pdres.to_json(orient='records')
        return valuejson
