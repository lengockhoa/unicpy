from mainapp.system.m_menu import Menu
from flask import Blueprint, jsonify, request, abort
menus = Blueprint('menus', __name__)


@menus.route('/menu/getlistactive', methods=['POST'])
def menu_getlistactive():
    if not request.json:
        abort(400)
    else:
        val = request.json
        menu = Menu()
        menu.user_code = val['user_code']
        result = menu.getlistactive()
        return result, 201


