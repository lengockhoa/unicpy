#!/bin/bash

# File nay nho set chmod de co the execute duoc
# sudo chmod +x /backend/run.sh

sudo kill $(ps aux | grep gunicorn | awk '{print $2}')
gunicorn --pythonpath=/backend --workers=5 --worker-connections=500 --worker-class=gevent --bind 0.0.0.0:5799 app:app

exit 0